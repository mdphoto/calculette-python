# Calculette python

Solution pour avoir une calculette python et bien plus sous Android

Nous allons installer Jupyter sur un téléphone sous Android. 
https://jupyter.org/

# Installation
- Installer TF-droid https://f-droid.org
- Avec f-droid installer termux et termux widget

### Dans Termux : 
- pkg update
- pkg upgrade
- setup-termux-storage
- Validez
- pkg install wget
- wget https://gitlab.com/mdphoto/calculette-python/raw/master/Jupyter-notebook.sh
- chmod +x jupyter-notebook.sh
- bash jupyter-notebook.sh

# Création du Racourci
- mkdir -p $HOME/.shortcuts
- pkg install nano ou vim ou emacs
- emacs .shortcuts/start-jupyter.sh
- Dans le fichier start-jupyter.sh
- exec jupyter notebook
- chmod +x start-jupyter.sh
- Créer un widget termux sur le bureau de votre Android
- selectionez start-jupyter.sh
- pour la premiere connexion copier l'url dan sl eterminal de termux
- et la coller dans votre navigateur
- dans le navigateur créer un racouci sur le bureau de http://localhost:8888

# Utilisation
- Lancer avec le widget start-jupyter.sh
- Cliquez sur le racourci du navigateur

# English

# Calculator python

Solution to having à calculator python in smartphone (Android) 
we install jupyter on android smartphone or tablette
https://jupyter.org/

# Install
- Install TF-droid https://f-droid.org
- In f-droid install termux and termux widget

### On Termux : 
- pkg update
- pkg upgrade
- setup-termux-storage
- Confirm ok
- pkg install wget
- wget https://gitlab.com/mdphoto/calculette-python/raw/master/Jupyter-notebook.sh
- chmod +x jupyter-notebook.sh
- bash jupyter-notebook.sh

# Creat a shortcut
- mkdir -p $HOME/.shortcuts
- pkg install nano or vim or emacs or your other like editor;
- emacs .shortcuts/start-jupyter.sh
- creat and open start-jupyter.sh file and type : exec jupyter notebook
- Save ans close file
- chmod +x start-jupyter.sh
- Creat a widget termux
- select start-jupyter.sh
- for first connexion copy url in termux termina http://localhost:8888....... 
- and past in navigator
- with navigator creat a shortcut http://localhost:8888

# Use
- Start start-jupyter.sh on widget
- and clik on navigator shortcut


