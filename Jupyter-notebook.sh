pkg upgrade
pkg install python python-dev libzmq-dev libcrypt-dev clang
pip3 install -U pip
pip3 install pyzmq --install-option="--zmq=/usr/lib"
pip3 install jupyter

#TODO: Clean up ~200 mb pulled by build...
apt remove python-dev libzmq-dev libcrypt-dev clang
apt autoremove
rm .cache ../usr/var/cache -rf
